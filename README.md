# ChemInformatics_Computational_Drug_Discovery

We will be reproducing a research article (by John S. Delaney) by applying Linear Regression to predict the solubility of molecules (i.e. solubility of drugs is an important physicochemical property in Drug discovery, design, and development).
